# oc-pagecache

Very simple plugin that installs spatie-pagecache and provides full-page caching for your page

### Usage

- Install (and run composer update)
- `php artisan vendor:publish`

