<?php namespace Viamage\PageCache;

use Backend;
use Spatie\ResponseCache\ResponseCacheServiceProvider;
use System\Classes\PluginBase;
use Illuminate\Contracts\Http\Kernel;

/**
 * PageCache Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'viamage.pagecache::lang.plugin.name',
            'description' => 'viamage.pagecache::lang.plugin.description',
            'author'      => 'Viamage',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(ResponseCacheServiceProvider::class);
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        /** @var \October\Rain\Foundation\Http\Kernel $kernel */
        $kernel = $this->app[Kernel::class];
        $kernel->pushMiddleware(       \Spatie\ResponseCache\Middlewares\CacheResponse::class);

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Viamage\PageCache\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'viamage.pagecache.some_permission' => [
                'tab' => 'viamage.pagecache::lang.plugin.name',
                'label' => 'viamage.pagecache::lang.permissions.some_permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'pagecache' => [
                'label'       => 'viamage.pagecache::lang.plugin.name',
                'url'         => Backend::url('viamage/pagecache/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['viamage.pagecache.*'],
                'order'       => 500,
            ],
        ];
    }

}
